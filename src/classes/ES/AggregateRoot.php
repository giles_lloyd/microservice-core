<?php
declare(strict_types=1);

namespace ES;

use Domain\Event\EventBus;
use Prooph\EventSourcing\AggregateChanged;
use Test\ReflectionTrait;

class AggregateRoot extends \Prooph\EventSourcing\AggregateRoot
{
	use ReflectionTrait;

	/**
	 * @var string
	 */
	protected $id;

	protected function __construct()
	{

		$this->id = (string)\Ramsey\Uuid\Uuid::uuid4();
	}

	/**
	 * @return string
	 */
	public function getID(): string
	{
		return $this->id;
	}

	public function recordThat(AggregateChanged $event): void
	{
		parent::recordThat($event);

		EventBus::instance()->publishEvent($event);
	}

	protected function aggregateId(): string
	{
		return $this->getID();
	}

	protected function apply(AggregateChanged $event): void
	{
		foreach ($event->payload() as $property => $value) {
			$this->id = $event->aggregateId();

			if (property_exists($this, $property)) {
				$this->setPrivateProperty($property, $value, $this);
			}
		}
	}
}
