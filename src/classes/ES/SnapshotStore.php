<?php
declare(strict_types=1);

namespace ES;

use Prooph\SnapshotStore\Pdo\PdoSnapshotStore;

class SnapshotStore
{
	/**
	 * @var PdoSnapshotStore
	 */
	private static $instance;

	private function __construct()
	{}

	public static function instance(): PdoSnapshotStore
	{
		if (!static::$instance) {
			static::$instance = new PdoSnapshotStore(
				\ORM::instance()->getPDO()
			);
		}

		return static::$instance;
	}
}
