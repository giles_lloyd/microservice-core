<?php

namespace Notification;

use Message\MessageBus;

class Notifier
{
    /**
     * @var MessageBus
     */
    private $messageBus;

    public function __construct(MessageBus $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function sendError(string $message, string $socketId = null): void
    {
        if ($socketId) {
            $this->sendMessage($socketId, 'flash.error', $message);
        }
    }

    public function sendSuccess(string $message, string $socketId = null): void
    {
        if ($socketId) {
            $this->sendMessage($socketId, 'flash.success', $message);
        }
    }

    private function sendMessage(string $socketId, string $type, string $message): void
    {
        $this->messageBus->async(
            'fmf-notification',
            [
                'role' => 'notification',
                'cmd' => 'publish',
                'payload' => [
                    'socketId' => $socketId,
                    'type' => $type,
                    'message' => $message,
                ],
            ]
        );
    }
}