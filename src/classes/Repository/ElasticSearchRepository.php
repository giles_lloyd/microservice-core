<?php
declare(strict_types=1);

namespace Repository;

abstract class ElasticSearchRepository
{
//	const INDEX = 'people_hr';

	/**
	 * @var \Elasticsearch\Client
	 */
	protected static $connection;

	/**
	 * @return \Elasticsearch\Client
	 */
	private function connection()
	{
		if (!static::$connection) {
			static::$connection = \Elasticsearch\ClientBuilder::create()
				->setHosts((new \Configuration('elasticsearch'))->get('elasticsearch'))
				->build();
		}

		return static::$connection;
	}

	/**
	 * @param $id
	 * @return array
	 */
	protected function getRawByID($id)
	{
		try {
			$params = [
				'index' => static::INDEX,
				'type' => static::TYPE,
				'id' => $id
			];

			$es = $this->connection();
			$response = $es->get($params);

			return $response['_source']['data'] ?: [];
		} catch (\Exception $e){
			// TODO Throw not found exception
		}
	}

	protected function getAllByType(string $type)
	{
		try {
			$params = [
				'index' => static::INDEX,
				'type' => $type,
				'body' => [
					'from' => 0,
					'size' => 500,
					'query' => ['match_all' => []],
				],
			];

			$es = $this->connection();
			$response = $es->search($params);

			return $this->parseHits($response);
		} catch (\Exception $e){
			return [];
			// TODO Throw not found exception
		}
	}

	/**
	 * @param int $id
	 * @param array $data
	 * @return array
	 */
	protected function storeJSON($id, array $data)
	{
		$params = [
			'index' => static::INDEX,
			'type' => static::TYPE,
			'id' => $id,
			'body' => json_encode([
				'data' => $data
			])
		];

		$es = $this->connection();

		$es->index($params);
	}

	public function search(array $query)
	{
		$result = $this->connection()
			->search([
				'index' => static::INDEX,
				'type' => static::TYPE,
				'body' => $query
			]);

		if (isset($result['hits']['total']) && ($result['hits']['total'] > 0)) {
			return $result['hits']['hits'];
		}

		return [];
	}

	public function clear()
	{
		foreach ($this->getAllByType(static::TYPE) as $id => $document) {
			$this->delete($id);
		}
	}

	protected function parseHits(array $response): array
	{
		$hits = [];

		foreach ($response['hits']['hits'] as $hit) {
			$hits[$hit['_id']] = $hit['_source']['data'];
		}

		return $hits;
	}

	public function delete($id)
	{
		try {
			$params = [
				'index' => static::INDEX,
				'type' => static::TYPE,
				'id' => $id
			];

			$es = $this->connection();

			$es->delete($params);
		} catch (\Exception $e){
			return [];
		}
	}
}