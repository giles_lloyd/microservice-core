<?php
declare(strict_types=1);

namespace Repository;

use Prooph\EventSourcing\Aggregate\AggregateRepository;
use Prooph\EventSourcing\EventStoreIntegration\AggregateTranslator;
use Prooph\EventSourcing\Aggregate\AggregateType;
use Prooph\EventStore\EventStore;
use Prooph\SnapshotStore\SnapshotStore;

class CQRSRepository extends AggregateRepository
{
	public function __construct(EventStore $eventStore, SnapshotStore $snapshotStore = null)
	{
		parent::__construct(
			$eventStore,
			AggregateType::fromAggregateRootClass(static::ENTITY_CLASS),
			new AggregateTranslator(),
			$snapshotStore,
			null,
			true
		);
	}

}
