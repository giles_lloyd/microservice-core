<?php
declare(strict_types=1);

namespace Repository;

use Prooph\EventSourcing\Aggregate\AggregateRepository;
use Prooph\EventSourcing\Aggregate\AggregateType;
use Prooph\EventSourcing\EventStoreIntegration\AggregateTranslator;
use Prooph\EventStore\EventStore;
use Prooph\SnapshotStore\SnapshotStore;

abstract class ProophRepository extends AggregateRepository
{
	public function __construct(EventStore $eventStore, SnapshotStore $snapshotStore)
	{
		parent::__construct(
			$eventStore,
			AggregateType::fromAggregateRootClass(static::ENTITY_CLASS),
			new AggregateTranslator(),
			$snapshotStore,
			null,
			true
		);
	}
}
