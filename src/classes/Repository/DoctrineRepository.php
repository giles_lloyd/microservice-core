<?php
namespace Repository;

use Doctrine\ORM\EntityManager;

abstract class DoctrineRepository
{
	/**
	 * @var EntityManager
	 */
	private $entity_manager;

	protected function getEntityManager() : EntityManager
	{
		!$this->entity_manager and	$this->entity_manager = \ORM::instance()->getEntityManager();

		return $this->entity_manager;
	}

	/**
	 * Use doctrine to persist a model in the DB
	 * @param obj $model Any new doctrine entity model
	 */
	public function saveNewModelToDB($model)
	{
		$this->getEntityManager()->persist($model);
		$this->getEntityManager()->flush();
	}

	/**
	 * Save all updates to managed entities
	 * @return bool
	 */
	public function saveChanges()
	{
		try{
			$this->getEntityManager()->flush();
			return true;
		}
		catch(\Exception $e){
			\Log::error($e->getMessage());
			return false;
		}
	}

	public function removeEntity($entity)
	{
		$this->getEntityManager()->remove($entity);
	}

	public function getAll() : array
	{
		return $this->getEntityManager()
			->getRepository(static::ENTITY_CLASS)
			->findAll() ?: [];
	}

	public function getEntityByID(String $entity_name, $entity_id, string $id_field = 'id')
	{
		return $this->getEntityManager()
			->getRepository($entity_name)
			->findOneBy(
				[
					$id_field => $entity_id,
				]
			);
	}

	public function getByFilters(array $filters, array $order_bys = []): array
	{
		return $this->getEntityManager()
			->getRepository(static::ENTITY_CLASS)
			->findBy($filters, $order_bys);
	}

	protected function softDelete(\SoftDeletable $entity)
	{
		$entity->setDeletedAt(new \DateTimeImmutable());
		$this->saveChanges();
	}
}