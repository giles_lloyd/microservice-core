<?php
declare(strict_types=1);

namespace DI;

use Base\BaseStatus;

class InjectionType extends BaseStatus
{
	const SHARED = 'share';
	const INSTANCE = 'add';
}