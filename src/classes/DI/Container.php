<?php
declare(strict_types=1);

namespace DI;

use League\Container\Container as LeagueContainer;

class Container
{
	/**
	 * @var LeagueContainer
	 */
	private static $instance;

	private function __construct()
	{
	}

	public static function instance(): LeagueContainer
	{
		if (!static::$instance) {
			static::$instance = new LeagueContainer();
			static::addClasses();
		}

		return static::$instance;
	}

	private static function addClasses()
	{
		$di_config = new \Configuration('di');

		foreach ($di_config->getAll() as $alias => $alias_config) {
			if (!class_exists($alias_config[0])) {
				throw new \Exception("Class '{$alias_config[0]}' does not exist");
			}

			static::addToContainer($alias, array_shift($alias_config), $alias_config);
		}
	}

	private static function addToContainer(string $alias, string $class, array $config)
	{
		$dependencies = array_shift($config);
		$type = $config[0] ?? InjectionType::INSTANCE;

		if ($dependencies) {
			static::addWithDependencies($type, $alias, $class, $dependencies);
		} else {
			static::$instance->$type($alias, $class);
		}
	}

	private static function addWithDependencies(string $method, string $alias, string $class, $dependencies)
	{
		if (is_array($dependencies)) {
			static::$instance->$method($alias, $class)->withArguments($dependencies);
		} else {
			static::$instance->$method($alias, $class)->withArgument($dependencies);
		}
	}
}