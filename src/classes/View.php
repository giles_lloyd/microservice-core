<?php
declare(strict_types=1);

class View
{
	/**
	 * @var string
	 */
	private $fileName;

	/**
	 * @var array
	 */
	private $data = [];

	private static $viewPaths = [];

	public function __construct(string $fileName, array $data = [])
	{
		$this->fileName = $fileName;
		$this->data = $data;
	}

	public function set(string $key, $value): View
	{
		$this->data[$key] = $value;

		return $this;
	}

	public function render(): string
	{
		extract($this->data);
		ob_start();
		require($this->getPath());
		$view = ob_get_contents();
		ob_end_clean();
		return $view;
	}

	private function getPath(): string
	{
		foreach ($this->getViewPaths() as $path) {
			$filePath = sprintf('%s/Delivery/%s/%s', INFRASTRUCTURE, $path, $this->fileName);

			if (file_exists($filePath)) {
				return $filePath;
			}
		}

		throw new RuntimeException('View not found: '.$this->fileName);
	}

	private function getViewPaths(): array
	{
		if (!static::$viewPaths) {
			static::$viewPaths = (new Configuration('views'))->get('paths');
		}

		return static::$viewPaths;
	}

	public function __toString(): string
	{
		return $this->render();
	}
}
