<?php
declare(strict_types=1);

namespace Domain\Event;

use Exception\ConfigNotFoundException;
use League\Container\Container;
use Prooph\Common\Messaging\FQCNMessageFactory;
use Prooph\EventStore\ActionEventEmitterEventStore;
use Prooph\EventStore\Pdo\PersistenceStrategy\PostgresAggregateStreamStrategy;
use Prooph\EventStore\Pdo\PostgresEventStore;
use Prooph\Common\Event\ProophActionEventEmitter;
use Prooph\ServiceBus\Plugin\Router\EventRouter;
use Prooph\EventStoreBusBridge\EventPublisher;
use Prooph\ServiceBus\EventBus as ESEventBus;

class EventBus
{
	/**
	 * @var EventBus
	 */
	private static $instance;

	/**
	 * @var ActionEventEmitterEventStore
	 */
	private static $event_store;

	/**
	 * @var ProophActionEventEmitter
	 */
	private static $event_emitter;

	/**
	 * @var Container
	 */
	private $di_container;

	/**
	 * @var DomainEventListener[]
	 */
	private $event_listeners = [];

	/**
	 * @var EventRouter
	 */
	private $event_router;

	/**
	 * EventBus constructor - singleton.
	 * @param Container $di_container
	 */
	private function __construct(Container $di_container)
	{
		$this->di_container = $di_container;

		if ($this->hasDBConnection()) {
			$this->event_router = new EventRouter();

			$eventBus = new ESEventBus(static::getEventEmitter());
			$eventPublisher = new EventPublisher($eventBus);
			$eventPublisher->attachToEventStore(static::getEventStore());
		}

		$this->event_listeners = $this->getEventsFromConfig();

		$this->hasDBConnection() and $this->event_router->attachToMessageBus($eventBus);
	}

	/**
	 * @return EventBus
	 * @throws \Exception
	 */
	public static function instance(): EventBus
	{
		if (!static::$instance) {
			throw new \Exception("Event bus has not been initialised");
		}

		return static::$instance;
	}

	public static function init(Container $di_container)
	{
		static::$instance = new static($di_container);
	}

	public static function getEventStore(): ActionEventEmitterEventStore
	{
		if (!static::$event_store) {
			static::$event_store = new ActionEventEmitterEventStore(
				new PostgresEventStore(
					new FQCNMessageFactory(),
					\ORM::instance()->getPDO(),
					new PostgresAggregateStreamStrategy()
				),
				static::getEventEmitter()
			);
		}

		return static::$event_store;
	}

	protected static function getEventEmitter(): ProophActionEventEmitter
	{
		if (!static::$event_emitter) {
			static::$event_emitter = new ProophActionEventEmitter();
		}

		return static::$event_emitter;
	}

	/**
	 * @param string $event
	 * @param DomainEventListener $listener
	 * @return EventBus
	 */
	public function addListener(string $event, DomainEventListener $listener): EventBus
	{
		$this->event_listeners[$event][] = $listener;

		return $this;
	}

	/**
	 * @param $event
	 */
	public function publishEvent($event)
	{
		if (isset($this->event_listeners[get_class($event)])) {
			foreach ($this->event_listeners[get_class($event)] as $listener) {
				$listener->handle($event);
			}
		}
	}

	private function getEventsFromConfig(): array
	{
		$event_listeners = [];

		$config = new \Configuration('eventbus');
		foreach ($config->getAll() as $event => $handlers) {
			foreach ($handlers as $handler) {
				if (is_array($handler)) {
					$this->addToEventRouter($event, $handler);
				} else {
					$event_listeners[$event][] = $this->getHandler($handler);
				}
			}
		}

		return $event_listeners;
	}

	private function addToEventRouter(string $event, array $handler)
	{
		if ($this->event_router) {
			$this->event_router->route($event)->to(
				[
					$handler[0],
					$handler[1] ?? 'handle'
				]
			);
		}
	}

	private function getHandler(string $handlerName)
	{
		if (class_exists($handlerName)) {
			return $handlerName;
		}

		return $this->di_container->get($handlerName);
	}

	private function hasDBConnection(): bool
	{
		try {
			new \Configuration('db');

			return true;
		} catch (ConfigNotFoundException $e) {
			return false;
		}

		return false;
	}
}
