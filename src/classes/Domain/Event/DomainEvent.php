<?php
declare(strict_types=1);

namespace Domain\Event;

abstract class DomainEvent
{
	/**
	 * @var \DateTimeImmutable
	 */
	protected $published_at;

	public function __construct()
	{
		$this->published_at = new \DateTimeImmutable();
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function publishedAt(): \DateTimeImmutable
	{
		return $this->published_at;
	}
}