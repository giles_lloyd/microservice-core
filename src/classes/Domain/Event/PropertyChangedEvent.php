<?php

declare(strict_types=1);

namespace Domain\Event;

use Prooph\EventSourcing\AggregateChanged;

class PropertyChangedEvent extends AggregateChanged
{
}
