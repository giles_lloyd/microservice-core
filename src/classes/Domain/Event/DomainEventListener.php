<?php
namespace Domain\Event;

interface DomainEventListener
{
	public function handle($event);
}