<?php
declare(strict_types=1);

namespace Exception;

use Throwable;

class ConfigNotFoundException extends \Exception
{
	public function __construct($message = "", $code = 0, Throwable $previous = null)
	{
		$message = $message ? "The config '{$message}' was not found" : "The requested config was not found";
		parent::__construct($message, $code, $previous);
	}
}