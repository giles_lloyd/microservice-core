<?php
declare(strict_types=1);

class Configuration
{
	/**
	 * @var string
	 */
	private $config_name;

	/**
	 * @var array
	 */
	private $config = [];

	public function __construct(string $config_name)
	{
		$this->config_name = $config_name;

		$this->config = $this->mergeConfigFiles(
			$this->getFilePaths($config_name)
		);
	}

	public function get(string $key, $default = null)
	{
		if ($key === $this->config_name) {
			return $this->config;
		}

		$value = $this->config;

		foreach (explode('.', $key) as $part) {
			if (!array_key_exists($part, $value)) {
				return $default;
			}

			$value = $value[$part];
		}

		return $value;
	}

	public function getAll() : array
	{
		return $this->config;
	}

	private function getFilePaths(string $config_name): array
	{
		return [
			sprintf('%s/config/%s.php', INFRASTRUCTURE, $config_name),
			sprintf('%s/config/%s/%s.php', INFRASTRUCTURE, getenv('ENV'), $config_name),
		];
	}

	private function mergeConfigFiles(array $file_paths): array
	{
		$config = [];
		$config_exists = false;

		foreach ($file_paths as $file) {
			if (file_exists($file)) {
				$config = array_merge($config, require $file);
				$config_exists = true;
			}
		}

		if (!$config_exists) {
			throw new \Exception\ConfigNotFoundException($this->config_name);
		}

		return $config;
	}
}