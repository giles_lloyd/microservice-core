<?php
declare(strict_types=1);

use League\Tactician\Container\ContainerLocator;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\CommandBus as TacticianCommandBus;

class CommandBus
{
	/**
	 * @var League\Tactician\CommandBus
	 */
	private static $instance;

	/**
	 * @var array|CommandHandlerMiddleware
	 */
	private static $commands = [];

	/**
	 * @var \League\Container\Container
	 */
	private static $di_container;

	private function __construct()
	{}

	public static function instance() : League\Tactician\CommandBus
	{
		if (!static::$instance) {
			static::$di_container = \DI\Container::instance();
			static::$instance = new TacticianCommandBus(
				[
					static::getCommandMap()
				]
			);
		}

		return static::$instance;
	}

	public static function addCommands(array $command_list)
	{
		if(static::$instance){
			throw new \Exception("It is too late to add new commands to the command bus");
		}

		static::$commands = array_merge(static::$commands, $command_list);
	}

	private static function getCommandMap(): CommandHandlerMiddleware
	{
		static::populateCommandsFromConfig();
		static::resolveCommandHandlers();

		return static::$commands;
	}

	private static function populateCommandsFromConfig()
	{
		$config = new \Configuration('commandbus');

		static::$commands += $config->getAll();
	}

	private static function resolveCommandHandlers()
	{
		static::$commands = new CommandHandlerMiddleware(
			new ClassNameExtractor(),
			new ContainerLocator(
				static::$di_container,
				static::$commands
			),
			new HandleInflector()
		);
	}
}