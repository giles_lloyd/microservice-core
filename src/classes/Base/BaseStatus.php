<?php
declare(strict_types=1);

namespace Base;

abstract class BaseStatus
{
	public static function all() : array
	{
		 return (new \ReflectionClass(static::class))->getConstants();
	}
}
