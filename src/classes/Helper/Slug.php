<?php

declare(strict_types=1);

namespace Helper;

class Slug
{
    public static function createFromString(string $input): string
    {
        return strtolower(
            trim(
                preg_replace('~[^0-9a-z]+~i', '-',
                    html_entity_decode(
                        preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1',
                            htmlentities(
                                str_replace('&', 'and', $input),
                                ENT_QUOTES,
                                'UTF-8'
                            )
                        ),
                        ENT_QUOTES,
                        'UTF-8'
                    )
                ),
                '-'
            )
        );
    }
}
