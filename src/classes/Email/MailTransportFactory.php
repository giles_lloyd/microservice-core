<?php
declare(strict_types=1);

namespace Email;

class MailTransportFactory
{
	/**
	 * @var MailTransportFactory
	 */
	private static $instance;

	/**
	 * @var array
	 */
	private $config = [];

	private function __construct()
	{
		$this->config = new \Configuration('email');
	}

	/**
	 * @return MailTransportFactory
	 */
	public static function instance(): MailTransportFactory
	{
		!static::$instance and static::$instance = new static();

		return static::$instance;
	}

	/**
	 * @return \Swift_SmtpTransport
	 */
	public function smtp(): \Swift_SmtpTransport
	{
		$transport = new \Swift_SmtpTransport(
			$this->config->get('smtp.host'),
			$this->config->get('smtp.port'),
			$this->config->get('smtp.security')
		);

		$this->config->get('smtp.user') and $transport->setUsername($this->config->get('smtp.user'));
		$this->config->get('smtp.password') and $transport->setPassword($this->config->get('smtp.password'));

		return $transport;
	}
}