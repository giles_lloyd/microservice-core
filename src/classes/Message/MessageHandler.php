<?php
declare(strict_types=1);

namespace Message;

use PhpAmqpLib\Message\AMQPMessage;

class MessageHandler
{
	/**
	 * @var array
	 */
	private $routes = [];

    /**
     * @var MessageBus
     */
	private $messageBus;

	public function __construct(MessageBus $messageBus)
    {
        $this->messageBus = $messageBus;
    }

	public function add(string $route, string $controller, string $method)
	{
		$this->routes[$route] = [new $controller(), $method];
	}

	public function processMessage(string $message): array
	{
		$payload = json_decode($message, true);
		$response = null;

		if ($route = $this->getRoute($payload)) {
			$controller = $route[0];
			$method = $route[1];

            $payload['socketId'] and $this->messageBus->setSocketId($payload['socketId']);

			$response = (new $controller())->$method(
				new MessagePayload($payload['payload'] ?? [], $payload['socketId'] ?? null)
			);
		}

		return [$response, $payload['socketId'] ?? null];
	}

	private function getRoute(array $payload)
	{
		$route = false;

		if (isset($payload['role'])) {
			$route_name = sprintf(
				'%s.%s',
				$payload['role'],
				$payload['cmd'] ?? 'execute'
			);

			isset($this->routes[$route_name]) and $route = $this->routes[$route_name];
		}

		return $route;
	}
}
