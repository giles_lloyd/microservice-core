<?php
declare(strict_types=1);

namespace Message;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MessageListener
{
	/**
	 * @var AMQPStreamConnection
	 */
	private $connection;

	/**
	 * @var \PhpAmqpLib\Channel\AMQPChannel
	 */
	private $channel;

	/**
	 * @var string
	 */
	private $queue;

	/**
	 * @var array
	 */
	private $exchanges = [];

	/**
	 * @var MessageHandler
	 */
	private $message_handler;

	public function __construct(\Configuration $config, MessageHandler $message_handler)
	{
		$this->connection = new AMQPStreamConnection(
			$config->get('host'),
			$config->get('port'),
			$config->get('user'),
			$config->get('password')
		);

		$this->queue = $config->get('queue');

		$this->channel = $this->connection->channel();
		list($queue, ,) = $this->channel->queue_declare($this->queue, false, false, false, false);

		$this->bindExchanges($config, $queue);

		$this->message_handler = $message_handler;
	}

	public function run(): void
	{
		$this->setupListener();

		while ($this->channel->is_consuming()) {
			$this->channel->wait();
		}
	}

	private function setupListener(): void
	{
		$this->channel->basic_consume(
			$this->queue,
			'',
			false,
			true,
			false,
			false,
			[$this, 'process']
		);
	}

	public function process(AMQPMessage $message)
	{
		try {
			list($response, $socketId) = $this->message_handler->processMessage($message->body);

			if ($response !== null) {
				$this->respond($message, $response, $socketId);
			}
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	private function bindExchanges(\Configuration $config, string $queue): void
	{
		foreach ($config->get('exchanges', []) as $exchange => $routingKey) {
			if (is_array($routingKey)) {
				foreach ($routingKey as $key) {
					$this->channel->exchange_declare($exchange, 'fanout', false, false, false);
					$this->channel->queue_bind($queue, $exchange, $key);
				}
			} else {
				// If no routingKeys are provided then the value is actually the exchange name
				$this->channel->exchange_declare($routingKey, 'fanout', false, false, false);
				$this->channel->queue_bind($queue, $routingKey);
			}
		}
	}

	private function respond(AMQPMessage $request, array $response, string $socketId = null)
	{
		$request->delivery_info['channel']->basic_publish(
			new AMQPMessage(
				$this->buildMessage($response, $socketId),
				['correlation_id' => $request->get('correlation_id')]
			),
			'',
			$request->get('reply_to')
		);
	}

	private function buildMessage(array $response, string $socketId = null): string
    {
        if ($socketId) {
            $response['socketId'] = $socketId;
        }

        return json_encode($response);
    }
}
