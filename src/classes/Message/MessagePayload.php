<?php
declare(strict_types=1);

namespace Message;

class MessagePayload
{
	/**
	 * @var array
	 */
	private $payload = [];

    /**
     * @var string|null
     */
	private $socketId;

	public function __construct(array $payload, string $socketId = null)
	{
		$this->payload = $payload;
		$this->socketId = $socketId;
	}

	public function getPayload(): array
	{
		return $this->payload;
	}

	public function get(string $key, string $default = null)
	{
		$value = $this->payload;

		foreach (explode('.', $key) as $part) {
			if (!array_key_exists($part, $value)) {
				return $default;
			}

			$value = $value[$part];
		}

		return $value;
	}

	public function getSocketId(): ?string
    {
        return $this->socketId;
    }

	public function toArray(): array
	{
		return $this->getPayload();
	}
}
