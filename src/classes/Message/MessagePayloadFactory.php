<?php
declare(strict_types=1);

namespace Message;

class MessagePayloadFactory
{
	public function fromScatterGather(array $responses): MessagePayload
	{
		$payload = [];
        $socketId = null;

		foreach($responses as $response) {
			$payload = array_merge($payload, $response);
		}

        if (isset($payload['socketId'])) {
            $socketId = $payload['socketId'];
            unset($payload['socketId']);
        }

		return new MessagePayload($payload, $socketId);
	}

	public function createPayload($payload): MessagePayload
    {
        $socketId = null;

        if (isset($payload['socketId'])) {
            $socketId = $payload['socketId'];
            unset($payload['socketId']);
        }

        return new MessagePayload($payload, $socketId);
    }
}
