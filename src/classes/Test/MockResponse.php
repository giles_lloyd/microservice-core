<?php

namespace Test;

class MockResponse extends \Application\Response\EntityResponse
{
	/**
	 * @var int
	 */
	protected $id;

	protected $name;

	protected $other_info;

	protected $created_at;

	protected $updated_at;

	public function __construct(int $id, string $name, string $other_info)
	{
		$this->id = $id;
		$this->name = $name;
		$this->other_info = $other_info;
		$this->created_at = new \DateTimeImmutable();
		$this->updated_at = clone $this->created_at;
	}

	/**
	 * @return int
	 */
	public function getID(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getOtherInfo(): string
	{
		return $this->other_info;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function getCreatedAt(): \DateTimeImmutable
	{
		return $this->created_at;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function getUpdatedAt(): \DateTimeImmutable
	{
		return $this->updated_at;
	}


}