<?php

namespace Test;

trait ReflectionTrait
{
	public function setPrivateProperty(string $property, $value, $object)
	{
		$reflectionClass = new \ReflectionClass(get_class($object));

		$reflectionProperty = $reflectionClass->getProperty($property);
		$reflectionProperty->setAccessible(true);
		$reflectionProperty->setValue($object, $value);
	}
}