<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class ORM
{
	protected static $_instance;

	protected $_entity_manager;
	protected static $_paths = [];

	protected function __construct()
	{
		$dbParams = new Configuration('db');

		$config = Setup::createXMLMetadataConfiguration(static::$_paths, true, null, null, false);
		$this->_entity_manager = EntityManager::create($dbParams->getAll(), $config);

		$this->_addSaveEvent();
	}

	public static function instance()
	{
		!static::$_instance and static::$_instance = new static();

		return static::$_instance;
	}

	/**
	 * @return PDO
	 */
	public function getPDO()
	{
		return $this->getEntityManager()
			->getConnection()
			->getWrappedConnection();
	}

	public function getEntityManager() : EntityManager
	{
		return $this->_entity_manager;
	}

	public static function addEntityPath($path)
	{
		if(static::$_instance){
			throw new \Exception("Paths must be added before ORM is initialized");
		}

		static::$_paths[] = $path;
	}

	public static function encryptor()
	{
		return static::$_encryptor;
	}

	public function _addSaveEvent()
	{
		$entity_manager = $this->getEntityManager();
		register_shutdown_function(function() use($entity_manager){
			if($entity_manager){
				try{
					$entity_manager->flush();
					return true;
				}
				catch(\Exception $e){
					\Log::error("DOCTRINE EXCEPTION: ".$e->getMessage());
				}
			}
		});
	}
}